'''
    @file Lab1.py
    @brief   FSM that cycles through different blinking LED patterns.
             Link to FSM Diagram: https://imgur.com/a/sqMIrPM / Link to Video: https://www.youtube.com/watch?v=4aoz-tYsuVk&ab_channel=ClaytonE
    @details Greets the user and instructs user on how to activate LED.
             The user can press the button to load the first pattern (square wave). Pressing the button again 
             changes states and the LED now displays a sine wave pattern. Press again to change states and display a
             sawtooth pattern. Once the button is pressed again the pattern changes back to a square wave and the cycle repeats. 
             To exit the program the user can press Ctrl+C.
             
    @author Clayton Elwell
    @author Zach Rannalli
    @author Tanner Hillman
    @date   October 8, 2021
'''

import time;
import pyb;
import math;
## Variable indicating whether button has been pressed or not.
ButtonPushed = False 


##  Configures Nucleo LED pinA5.
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
## Configures Nucleo timer at high frequency.
tim2 = pyb.Timer(2, freq = 20000)
## Associates timer and LED pin.
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)



# Function that indicates if button has been pressed
def onButtonPressFCN(IRQ_src):
    ''' 
        @brief A callback function that runs when the button is pressed.
        @details When the button is pressed on the Nucleo the variable ButtonPushed switches to true (high).
        @param IRQ_src detects button presses by checking for high-to-low transition (falling edge) on pin 13.
    '''
    
    global ButtonPushed
    ButtonPushed = True

## Sets up Button B1
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)

   

try: # try a button Push if its already configured an error will occur so we scip it
           ##  Assigns an object variable to a pin, which is the button (B1) on the Nucleo board, which can be used for user input.
           ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
    #sets up PIN/LED/Timer
except:
        
       print('')



if __name__ =='__main__':

    ##  The current state of the FSM
    #   Defines hardware state and loops through code until a condition to change the state occurs.
    State = 0
    ##  Keeps track of occurred cycles in the FSM
    runs = 0
    ##  Tracks of current time
    tcur = -1
    ##  Tracks of a single instance of time
    to = -1  
    ##  Value between 0-100 that controls LED brightness
    #   Integer that is used as a variable to define LED brightness.
    P = 0
         
    while (True):
        #Run FSM unless Ctrl+c is hit
        try:
            tcur = time.ticks_ms()
            
            ##  Tracks the time difference the current time (tcur) and a single instance in time (to)
            delt = time.ticks_diff(tcur,to)
            
            if (State == 0):
                #Run the State 0 code
                print('Welcome,\n'
                      'Click the blue button (B1 on the Nucleo) to cycle through 3 patterns: \nSquare, Sine, and Sawtooth.'
                      '\nPress Ctrl+C to stop.')
                print('\nPress blue button to start.')
                
                State = 1; # Transisions to state 1
                
            elif (State == 1):
                #Run the State 1 code
                
                if ButtonPushed == True:
                    State = 2; # Transisions to state 2
                    to = tcur;
                    print('Square Wave Pattern Selected')
                    
            elif (State == 2):
                # Run the State 2 code       
                
                # Generates Square wave response with a period of 1000 ms
                # We first divide each instance by half seconds and round down so every number is odd if even
                # Modulus makes every odd or even a 1 or 0 
                # Adject the amplitude to 100
                P = (((delt) // 500) % 2)*100
                
                
                if ButtonPushed == True:
                    State = 3 # Transisions to state 3
                    to = tcur;
                    print('Sine Wave Pattern Selected')
                    
            elif (State == 3):
                # Run the State 3 code
                
                # Generates Sine wave response with a period of 10000 ms
                # Adjust the peak to peak amplitude to 100 make the center of the sine wave 50
                P = 50*math.sin(2*math.pi*delt/10000) + 50

                if ButtonPushed == True:
                    State = 4  # Transisions to state 4
                    to = tcur
                    print('Sawtooth Wave Pattern Selected')
                    
            elif (State == 4):
                # Run the State 4 code
                
                # Generates Sawtooth wave responce with a period of 1000 ms
                # delt increase P at a constant slope
                # Modulus makes every instance that exceeds 1s or 1000ms restart
                # Adject the ampletude to 100 by dividing by 10
                P = ((delt) % 1000) /10
                             
                if ButtonPushed == True:
                    State = 2  # Transitions to state 2
                    to = tcur;
                    print('Square Wave Pattern Selected')
                
            
            ButtonPushed = False
            t2ch1.pulse_width_percent(P) # Command LED
            runs += 1       # increments runs
        
        #If there is an interuption break
        except KeyboardInterrupt:
            break
        
    print('Program Terminating')


    

        
    