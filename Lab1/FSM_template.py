# -*- coding: utf-8 -*-
"""
Created on Thu Sep 30 12:20:14 2021
@file   FSM_template.py
@brief  FSM Skeleton Code
@details This file will be used to demostrate one 
         method for implementing a finite state machine in Python

@author: celwe
"""




import pyb


pinC13 = pyb.Pin (pyb.Pin.cpu.C13)



if __name__ == '__main__':

    ## The next state to run as the FSM iterates
    state = 0
   ## Number of iterations of the FSM
    runs = 0
    
    
    while(True):
        try:
            #onButtonPressFCN(IRQ_src)
            if (state == 0):
                # run state 0
                print("In State 0")
                
                state = 1       #TRANSITION TO STATE 1
                
            elif(state==1):
                #run state 1
                
                print("In State 1")
                
                state = 2       #TRANSITION TO STATE 2
                
            elif(state==2):
                #run state 1
                
                print("In State 2")
                
                state = 3      #TRANSITION TO STATE 3
            
            elif(state==3):
                #run state 1
                
                print("In State 3")
                
                state = 4       #TRANSITION TO STATE 4
            
            elif(state==4):
                #run state 1
                
                print("In State 4")
                
                state = 2       #TRANSITION TO STATE 2
          #  runs += 1           # Increment run counter  
          #  time.sleep(1)       # Delay 1 second
            
        except KeyboardInterrupt:
            break
        
    print('Program Terminating')
    
    