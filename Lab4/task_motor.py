'''@file       task_motor.py
   @brief
   @details
   @author     Clayton Elwell
   @author     Zach Rannalli
   @date
'''

import utime
import pyb
#import motor_driver

pinB4 = pyb.Pin(pyb.Pin.cpu.B4)
pinB5 = pyb.Pin(pyb.Pin.cpu.B5)

pinB0 = pyb.Pin(pyb.Pin.cpu.B0)
pinB1 = pyb.Pin(pyb.Pin.cpu.B1)

#Index references for shared motor array
# Encoder number
E_N = 0
# Encoder Position
POS = 1
# Encoder Velocity
VEL = 2
# Assigned Duty Cycle
DUTY = 3
# Encoder zeroing Boolean Value
ZERO = 4
# Disables motor fault trigger
NO_FAULT = 5

class TaskMotor():
    
    '''@brief      Motor task that executes motor driver methods
       @details    Tracks encoder position and velocity; zeros encoder if ZERO is true
       
    '''
    
    def __init__(self, period, Shares, motor_drv):
        
        '''@brief      
           @details    
           @param       Period
           @param       Shares
           @param       motor_drv
        '''
        
        ## Defines the encoder overflow period
        self.period = period
        
        ## Current time + 1 period
        self.next_time = self.period + utime.ticks_ms()
        
        ## Attribute that communicates with the shared motor array;
        ## [E_N, POS, VEL, DUTY, ZERO, NO_FAULT]
        self.Shares = Shares
        
        ## Attribute associated with the motor driver 
        self.motor_drv = motor_drv
        
        
        if self.Shares.read(E_N) == 1:
            ## Attribute associated with the motor object
            self.motor = self.motor_drv.motor(pinB4, pinB5, 1, 2)
        elif self.Shares.read(E_N) == 2:
            
            self.motor = self.motor_drv.motor(pinB0, pinB1, 3, 4)
            
        self.motor_drv.enable()
        self.motor.set_duty(self.Shares.read(DUTY))
        
    def run(self):
         '''@brief      Motor task that executes motor driver methods
            @details    Tracks encoder position and velocity; zeros encoder if ZERO is true
       
         '''
         
         if (utime.ticks_diff(utime.ticks_ms(), self.next_time) >= 0):
             
             self.next_time += self.period
             if(self.Shares.read(NO_FAULT)):
                 self.motor_drv.enable()
                 #print('Motor fault has been disabled')
                 self.Shares.write(DUTY, 0)
                 self.Shares.write(NO_FAULT, False)
             self.motor.set_duty(self.Shares.read(DUTY))
             