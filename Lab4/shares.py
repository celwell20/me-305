''' @file       shares.py
    @brief      Task sharing ibrary implementing both shares and queues.
    @details    Implements a very simple interface for sharing data between
                multiple tasks.
'''

class ShareMotor:
    '''@brief     A shared variable for the motor
       @details   Access with read(); change with write()
    '''
    
    def __init__(self, initial_array):
        '''@brief     Constrcuts a shared variable
           @details
           @param     initial_array   An optional initial array for the 
                                   shared  variable
           '''    
        self._bufferArray = initial_array
    
    
    def write(self, index, item):
        '''@brief     Updates the value of the shared array
           @details   
           @param     index  The index of the specific array value to be changed
           @param     item The new value for the specific array index
           '''
    
        self._bufferArray[index] = item
    
    def read(self, index):
        '''@brief     A shared variable for the motor
           @details   
           @param     index The index of the variable to be returned
           @return    The value of the shared array variable
        '''
        
        return self._bufferArray[index]

    def read_all(self):
        '''@brief     Access the value of the shared array
           @details   Access with read(); change with write()
           @return    The value of the entire shared array
        '''
        return tuple(self._bufferArray)
        

class Share:
    ''' @brief      A standard shared variable.
        @details    Values can be accessed with read() or changed with write()
    '''
    def __init__(self, initial_value=None):
        ''' @brief      Constructs a shared variable
            @param      initial_value An optional initial value for the 
                                      shared variable.
        '''
        self._buffer = initial_value
    
    def write(self, item):
        ''' @brief      Updates the value of the shared variable
            @param item The new value for the shared variable
        '''
        self._buffer = item
        
    def read(self):
        ''' @brief      Access the value of the shared variable
            @return    The value of the shared variable
        '''
        return self._buffer

class Queue:
    ''' @brief      A queue of shared data.
        @details    Values can be accessed with placed into queue with put() or
                    removed from the queue with get(). Check if there are
                    items in the queue with num_in() before using get().
    '''
    def __init__(self):
        ''' @brief              Constructs an empty queue of shared values
        '''
        self._buffer = []
    
    def put(self, item):
        ''' @brief      Adds an item to the end of the queue.
            @param item The new item to append to the queue.
        '''
        self._buffer.append(item)
        
    def get(self):
        ''' @brief      Remove the first item from the front of the queue
            @return     The value of the item removed
        '''
        return self._buffer.pop(0)
    
    def num_in(self):
        ''' @brief      Find the number of items in the queue. Call before get().
            @return     The number of items in the queue
        '''
        return len(self._buffer)