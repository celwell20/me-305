# -*- coding: utf-8 -*-
"""@file Menu.py
@brief ME 305 lab assignments and homework assignments
@mainpage
      


                

@section sec_intro    Introduction
                      Links to the repository of various source codes and Lab documentation
                      
@section sec_links    Links to coursework
         Source Code Repository:   https://bitbucket.org/celwell20/me-305/src/master/ <br>
         <br>
         <B>Lab0x01:</B><br> Documentation:   https://celwell20.bitbucket.io/Lab1/ <br>
         Finite State Machine: https://imgur.com/a/sqMIrPM <br> LED Demonstration:  https://www.youtube.com/watch?v=4aoz-tYsuVk&ab_channel=ClaytonE <br>
         <br>
         <B>Lab0x02:</B><br> Documentation:   https://celwell20.bitbucket.io/Lab2/ <br>
         Finite State Machine: https://imgur.com/a/lgxKuXC <br>
         <br>
         <B>Lab0x03:</B><br>  Documentation: https://celwell20.bitbucket.io/Lab3/ <br>
         @ref lab_3_page 
         <br>
         <br>
         <B>Lab0x04:</B><br> Documentation: https://celwell20.bitbucket.io/Lab4/ <br>
         @ref lab_4_page
         <br>
         <br>
         <B>Homework 2:</B><br>
         @ref hw_2_page
         <br>
         <br>
         <B>Homework 3:</B><br>
         @ref hw_3_page
         <br>
         <br>
         <B>Lab 0x0FF (Term Project):</B><br> Documentation: https://celwell20.bitbucket.io/LabFF/ <br>
         Control Description and User Interface Demonstration: https://www.youtube.com/watch?v=xpG04apApBM&ab_channel=ClaytonE <br>
         Plate Balancing Demonstration: https://www.youtube.com/watch?v=oHe4kN9KipQ&ab_channel=ClaytonE <br>
         Ball Balancing Demonstration: https://www.youtube.com/watch?v=-rCVxf9tghA&ab_channel=ClaytonE <br>
         @ref FF_page
         
@author  Clayton Elwell






"""

