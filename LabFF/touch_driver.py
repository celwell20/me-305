'''@file   touch_driver.py
   @brief  Scans the x, y, and z components of the resistive touch panel and relays those scanned values
   @details Driver that scans the components of the resistive touch panel and relays those scanned values to the touch panel task.
   @author  Clayton Elwell
   @author Zachary Rannalli
   @date   December 9, 2021
'''

import pyb
import utime
import micropython


# pinXM = pyb.Pin()

# pinYP = pyb.Pin()


#print(str(actualPoints))

#print(str(actualPoints[0,0]))
#print(str(actualPoints[1,1]))



# ADCym = pyb.ADC(pinYM)

# ADCxp = pyb.ADC(pinXP)


class TouchPanel:
    '''@brief IMU driver objcet
       @details   Assigns pins to collect touch panel data
    '''
    
    
    
    
    def __init__(self):
        '''@brief   Constructs a TouchPanel object
           @details Coonstructes a TouchPanel object and various attributes associated with the pins that are used to determine where the platform is being
           touched. 
        '''
        ## Attribute that allows one to note the time on the timer 
        self.setTime = utime.ticks_us
        ## Attribute associated with the y_minus voltage node
        self.YM = pyb.Pin(pyb.Pin.cpu.A0)
        ## Attribute associated with the x_plus voltage node
        self.XP = pyb.Pin(pyb.Pin.cpu.A7)
        ## Attribute associated with the x_minus voltage node
        self.XM = pyb.Pin(pyb.Pin.cpu.A1)
        ## Attribute associated with the t_plus voltage node
        self.YP = pyb.Pin(pyb.Pin.cpu.A6)
       # ADCxm = pyb.ADC(pyb.Pin.cpu.A1)
       # ADCyp = pyb.ADC(pyb.Pin.cpu.A6)
        ## Allows one to set the pin output to a high/low output
        self.out = pyb.Pin.OUT_PP
        ## Allows one the pin as an input
        self.IN = pyb.Pin.IN
        ## Attribute associated with the width of the touch panel
        self.width = 176
        ## Attribute associated with the length of the touch panel
        self.length = 100
       ## Attribute associated with pin A1 on the Nucleo
        self.A0 = pyb.Pin.cpu.A0
        ## Attribute associated with pin A2 on the Nucleo
        self.A1 = pyb.Pin.cpu.A1
        ## Attribute associated with pin A6 on the Nucleo
        self.A6 = pyb.Pin.cpu.A6
        ## Attribute associated with pin A7 on the Nucleo
        self.A7 = pyb.Pin.cpu.A7
        ## Attribute that sets the pin output to H/L
        self.out = pyb.Pin.OUT_PP
        ## Attribute that sets the pin as an input
        self.inn = pyb.Pin.IN
        ## Attribute that assigns a new pin
        self.pin = pyb.Pin
        ## Attribute that assigns a new ADC
        self.ADC = pyb.ADC
        
        self.pin(self.A0,self.out,value = 1)
        self.pin(self.A7,self.out,value = 0)
        self.ADC(self.A1)
        self.ADC(self.A6)
        ## Attribute that sleeps for microsecond increments
        self.wait = utime.sleep_us
        
        
        
        
        (self.Kxx,self.Kxy,self.Kyx,self.Kyy,self.x0,self.y0) = (1,0,0,1,0,0)
            
        
        
        # self.t = self.setTime()
        
    # @micropython.native    
    # def update(self):
    #     '''@brief
    #        @details
    #     '''
    #     return self.callScan()
    
    
    def setCal(self, cal):
        '''@brief  Assigns calibration coefficient values
           @details Assigns the calibration coefficient values that were derived in lecture to correct the placement of the RT panel on the platform.
        '''
        (self.Kxx,self.Kxy,self.Kyx,self.Kyy,self.x0,self.y0) = cal
        
    
    @micropython.native
    def allScan(self):
        '''@brief  Scans for all components of the touch panel.        
           @details Scans all components of the touch panel and multiplies by the gain coefficients to calculate the adjusted x and y positions on the platform.
           @return  The adjusted x and y positions on the platform [mm] and a Boolean indiciating if there is anything in contact with the platform.
        '''
        
        # Scanning for ADC_x
        # pinXM = pyb.Pin(self.XM, self.out, value = 0)
        # pinXP = pyb.Pin(self.XP, self.out, value = 1)
        
        # pinYP = pyb.Pin(self.YP, self.IN)
        
        # ADCym = pyb.ADC(self.YM)
        # ADCyp = pyb.ADC(pinYP)
        
        # utime.sleep_us(5)
        
        # ADCx = ADCym.read()
        
        # # Scanning for ADC_y
        # pinYP = pyb.Pin(self.YP, self.out, value = 1)
        # pinYM = pyb.Pin(self.YM, self.out, value = 0)
       
        # pinXP = pyb.Pin(self.XP, self.IN)
       
        # ADCxm = pyb.ADC(self.XM)
        # ADCxp = pyb.ADC(pinXP)
       
        # utime.sleep_us(5)
       
        # ADCy = ADCxm.read()       
        
        # #Scanning for if the ball is in contact with the touch panel
        # pinYP = pyb.Pin(self.YP, self.out, value = 1)
        # pinXM = pyb.Pin(self.XM, self.out, value = 0)
        
        # pinXP = pyb.Pin(self.XP, self.IN)
        
        # ADCym = pyb.ADC(self.YM)
        # ADCxp = pyb.ADC(pinXP)
        
        # utime.sleep_us(5)
        
        # z = 20 < ADCym.read() < 4050
        
        
        
        # # making X matrix for calibration
        # x = ADCx*self.Kxx + ADCy*self.Kxy + self.x0
        # y = ADCx*self.Kyx + ADCy*self.Kyy + self.y0
        
        
        self.pin(self.A0,self.out,value = 1)
        self.wait(4)
        ADCxm = self.ADC(self.A1)
        z = ADCxm.read() > 69
        self.wait(4)
        self.pin(self.A6,self.out,value = 0)
        self.wait(4)
        ADCxp = self.ADC(self.A7)        
        yr = ADCxp.read()
        self.wait(4)
        self.pin(self.A1,self.out,value = 1)
        self.pin(self.A7,self.out,value = 0)
        self.wait(4)
        ADCym = self.ADC(self.A0)
        self.pin(self.A6,self.inn)
        xr = ADCym.read() 
        x = xr*self.Kxx+yr*self.Kxy+self.x0
        y = xr*self.Kyx+yr*self.Kyy+self.y0
        
        
        #X_matrix = np.array([ADCx, ADCy, 1])
        #print(self.calibMatrix)
        #print(X_matrix)
        #out = np.dot(X_matrix, self.calibMatrix)#.flatten()
        #print(out)
       # print((x, y))
      #  print((x,y), z)
        return (x, y, z)
    
   
           
   
    
        
    
       
            
    def readPos_Vel(self):
        '''@brief   Reads the current positions and velocities of the contact point
           @return  The attributes associated with the current position and velocity of the contact point.
        '''
        
        return(self.x, self.y, self.Vx, self.Vy)
     
    
    

            
    
    