# -*- coding: utf-8 -*-
"""@file    
@author: celwe
"""

import pyb
#import utime

#if __name__ == '__main__':

pinSleep = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
pinIN1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)
pinIN2 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)

 ##  Timer for IN1 and IN2 pins
tim1 = pyb.Timer(3, freq = 20000)
       ## Assigning the pin1 parameter to the STM32L476RG channel 1
t1ch1 = tim1.channel(1, pyb.Timer.PWM, pin=pinIN1)
       ## Assigning the pin2 parameter to the STM32L476RG channel 2
t1ch2 = tim1.channel(2, pyb.Timer.PWM, pin=pinIN2)

pinSleep.high()
#pinIN2.high()
#pinIN1.low()

while(True):

    try:
        t1ch1.pulse_width_percent(30)
        t1ch2.pulse_width_percent(0)
        
    except KeyboardInterrupt:
            
        t1ch1.pulse_width_percent(0)   



