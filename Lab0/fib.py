# -*- coding: utf-8 -*-
"""@file fib.py
@author: celwe
"""



# This function file allows the user to input an arbitrary integer input and the function
# will output the corresponding Fibonnacci number at that index.

# The acceptable inputs to this function are integers that are greater than or 
# equal to zero. Special characters, negative integers, and strings will not be accepted.


def fib(intidx):
    
    F = (intidx+1)*[0]  # Predefining a list to contain the sequence of Fib numbers.
    F[1] = 1            # Setting the 1 index of the list equal to one
        
    if intidx == 0:     # If statement for a requested index of zero
        return 0
    elif intidx == 1:   # If statement for a requested index of 1
        return F[1]
    elif intidx > 1:    # If statement for a requested index > 1
            
        for i in range(2, intidx+1):    # For loop that calculautes the Fib numbers when the 
                                        # requested index is greater than 1.
            F[i] = F[i-1] + F[i-2]      # Calculating the i-th value of the Fib sequence based
        return F[intidx]                # the previous two values.
    
    

if __name__ == '__main__':
    while True:                         # Setting up an infinite while loop
        idx = input('Choose an index: ')
    
        try:                            # Testing inputting index to ensure it is valid
            intidx = int(idx)           # Index must be an integer
            if intidx < 0:              # Index must be positive
                print('Index must be a positive value')
                continue
        except:
            print('Please enter an integer value')
            continue
    
        else:
            print('Fibonacci number at ''index {:} is {:}.'.format(idx,fib(intidx)))
            exit = input('Enter q to quit or press Enter to continue: ')
            if exit == 'q':             # Allows user to exit while loop at his/her discretion
                break
            else:                       # Allows user to continue calculating Fib numbers
                continue