'''@file ResultsHW2.py

@page hw_2_page B&P Simulation Hand Calculations

Here are the hand calculations to derive the equations of motion for the ball and plate platform simulation.
            
Kinematics
    \image html hw2p1.jpg "Ball and plate kinematics part 1"
More Kinematics
    \image html hw2p1.5.PNG "Ball and plate velocity and acceleration vectors"
    <br>
Kinetics
    \image html hw2p2.jpg "Ball and plate kinetics part 1"
More Kinetics    
    \image html hw2p3.jpg "Ball and plate kinetics part 2; ball kinetics"
<br>
Equations of Motion
    \image html hw2p4.jpg "Equations of motion"     
    
'''
